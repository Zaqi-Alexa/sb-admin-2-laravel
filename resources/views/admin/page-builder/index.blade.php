@extends('layout.backend.app',[
'title' => 'Manage Page',
'pageTitle' =>'Manage Page',
])

@push('css')
<link href="{{ asset('template/backend/sb-admin-2') }}/vendor/datatables/dataTables.bootstrap4.min.css"
    rel="stylesheet">
@endpush

@section('content')
<div class="notify"></div>

<div class="card">
    <div class="card-header">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-modal">
            Tambah Data
        </button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Layout</th>
                        <th>Route</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal Create -->
<div class="modal fade" id="create-modal" tabindex="-1" role="dialog" aria-labelledby="create-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="create-modalLabel">Create Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="createForm">
                <div class="modal-body">
                    <input type="hidden" name="layout" value="master">
                    <input type="hidden" name="locale" value="en">
                    <div class="form-group">
                        <label for="page_name"><b>Name</b></label>
                        <input type="text" required="" id="page_name" name="name" placeholder="Page Name"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title"><b>Page Title</b></label>
                        <input type="text" required="" id="title" name="title" placeholder="Page Title"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="route"><b>Route</b></label>
                        <input type="text" required="" id="route" name="route" placeholder="/Route"
                            class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-store">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Create -->

<!-- Modal Edit -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-modalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editForm">
                <div class="modal-body">
                    <input type="hidden" required="" id="id_edit" name="id" class="form-control">
                    <input type="hidden" name="layout" id="layout_edit" value="master">
                    <input type="hidden" name="locale" id="locale_edit" value="en">
                    <div class="form-group">
                        <label for="page_name"><b>Name</b></label>
                        <input type="text" required="" id="page_name_edit" name="name" placeholder="Page Name"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title"><b>Page Title</b></label>
                        <input type="text" required="" id="title_edit" name="title" placeholder="Page Title"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="route"><b>Route</b></label>
                        <input type="text" required="" id="route_edit" name="route" placeholder="/Route"
                            class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-update">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Edit -->

<!-- Destroy Modal -->
<div class="modal fade" id="destroy-modal" tabindex="-1" role="dialog" aria-labelledby="destroy-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="destroy-modalLabel">Yakin Hapus ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger btn-destroy">Hapus</button>
            </div>
        </div>
    </div>
</div>
<!-- Destroy Modal -->

@stop

@push('js')
<script src="{{ asset('template/backend/sb-admin-2') }}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('template/backend/sb-admin-2') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('template/backend/sb-admin-2') }}/js/demo/datatables-demo.js"></script>

<script type="text/javascript">
    $(function () {

        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('page-builder.index') }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'layout',
                    name: 'layout'
                },
                {
                    data: 'route',
                    name: 'route'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: true
                },
            ]
        });
    });


    // Reset Form
    function resetForm() {
        $("[name='name']").val("")
        $("[name='email']").val("")
        $("[name='password']").val("")
    }
    //

    // Create 

    $("#createForm").on("submit", function (e) {
        e.preventDefault()

        $.ajax({
            url: "/admin/page-builder",
            method: "POST",
            data: $(this).serialize(),
            success: function () {
                $("#create-modal").modal("hide")
                $('.data-table').DataTable().ajax.reload();
                flash("success", "Data berhasil ditambah")
                resetForm()
            }
        })
    })

    // Create

    // Edit & Update
    $('body').on("click", ".btn-edit", function () {
        var id = $(this).attr("id")

        $.ajax({
            url: "/admin/page-builder/" + id + "/edit",
            method: "GET",
            success: function (response) {
                // console.log(response)
                $("#edit-modal").modal("show")
                $("#id_edit").val(response.id)
                $("#layout_edit").val(response.layout)
                $("#locale_edit").val(response.locale)
                $("#page_name_edit").val(response.page_name)
                $("#title_edit").val(response.title)
                $("#route_edit").val(response.route)
            }
        })
    });

    $("#editForm").on("submit", function (e) {
        e.preventDefault()
        var id = $("#id").val()

        $.ajax({
            url: "/admin/page-builder/" + id,
            method: "PATCH",
            data: $(this).serialize(),
            success: function () {
                $('.data-table').DataTable().ajax.reload();
                $("#edit-modal").modal("hide")
                flash("success", "Data berhasil diupdate")
            }
        })
    })
    //Edit & Update

    $('body').on("click", ".btn-delete", function () {
        var id = $(this).attr("id")
        $(".btn-destroy").attr("id", id)
        $("#destroy-modal").modal("show")
    });

    $(".btn-destroy").on("click", function () {
        var id = $(this).attr("id")

        $.ajax({
            url: "/admin/page-builder/" + id,
            method: "DELETE",
            success: function () {
                $("#destroy-modal").modal("hide")
                $('.data-table').DataTable().ajax.reload();
                flash('success', 'Data berhasil dihapus')
            }
        });
    })

    function flash(type, message) {
        $(".notify").html(`<div class="alert alert-` + type + ` alert-dismissible fade show" role="alert">
                              ` + message + `
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>`)
    }

</script>
@endpush
