<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DataTables;
use DB;

use PHPageBuilder\Theme;
use PHPageBuilder\Modules\GrapesJS\PageRenderer;
use PHPageBuilder\Repositories\PageRepository;

class PageBuilderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('pagebuilder__pages')
                    ->select('pagebuilder__pages.id', 'pagebuilder__pages.name', 'pagebuilder__pages.layout', 'pagebuilder__page_translations.route')
                    ->join('pagebuilder__page_translations', 'pagebuilder__pages.id', '=', 'pagebuilder__page_translations.page_id')
                    ->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<div class="row"><a href="'.$row->route.'" id="'.$row->id.'" class="btn btn-sm ml-2 btn-info">Preview</a>';
                        $btn .= '<a href="'.route('pagebuilder.build',$row->id).'" class="btn btn-sm ml-2 btn-secondary">Build</a>';
                        $btn .= '<a href="javascript:void(0)" id="'.$row->id.'" class="btn btn-primary btn-sm ml-2 btn-edit">Edit</a>';
                        $btn .= '<a href="javascript:void(0)" id="'.$row->id.'" class="btn btn-danger btn-sm ml-2 btn-delete">Delete</a></div>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('admin.page-builder.index');
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $pages = DB::table('pagebuilder__pages')->insertGetId([
            'name' => $request->name,
            'layout' => $request->layout
        ]);

        DB::table('pagebuilder__page_translations')->insert([
            'locale' => $request->locale,
            'title' => $request->title,
            'route' => $request->route,
            'page_id' => $pages
        ]);
    }

    public function show($id)
    {
        //   
    }

    public function edit($id)
    {
        $edit = DB::table('pagebuilder__pages')
                    ->select('pagebuilder__pages.id', 'pagebuilder__pages.name as page_name', 'pagebuilder__pages.layout', 'pagebuilder__page_translations.route', 'pagebuilder__page_translations.locale', 'pagebuilder__page_translations.title', 'pagebuilder__page_translations.route')
                    ->join('pagebuilder__page_translations', 'pagebuilder__pages.id', '=', 'pagebuilder__page_translations.page_id')
                    ->where('pagebuilder__pages.id', '=', $id)
                    ->first();
        return response()->json($edit);
    }

    public function update(Request $request)
    {
        $pages = DB::table('pagebuilder__pages')
        ->where('id', '=', $request->id)
        ->update([
            'name' => $request->name,
            'layout' => $request->layout
        ]);

        DB::table('pagebuilder__page_translations')
        ->where('page_id', '=', $request->id)
        ->update([
            'locale' => $request->locale,
            'title' => $request->title,
            'route' => $request->route
        ]);
    }

    public function destroy($id)
    {
        DB::table('pagebuilder__pages')->where('id', '=', $id)->delete();
        DB::table('pagebuilder__page_translations')->where('page_id', '=', $id)->delete();
    }

    public function build($pageId = null) {
        $route = $_GET['route'] ?? null;
        $action = $_GET['action'] ?? null;
        $pageId = is_numeric($pageId) ? $pageId : ($_GET['page'] ?? null);
        $pageRepository = new \PHPageBuilder\Repositories\PageRepository;
        $page = $pageRepository->findWithId($pageId);

        $phpPageBuilder = app()->make('phpPageBuilder');
        $pageBuilder = $phpPageBuilder->getPageBuilder();

        $customScripts = view("admin.page-builder.scripts")->render();
        $pageBuilder->customScripts('head', $customScripts);

        $pageBuilder->handleRequest($route, $action, $page);
    }

    public function preview($pageId)
    {
        $theme = new Theme(config('pagebuilder.theme'), config('pagebuilder.theme.active_theme'));
        $page = (new PageRepository)->findWithId($pageId);
        $pageRenderer = new PageRenderer($theme, $page);
        $html = $pageRenderer->render();
        return $html;
    }

    public function uri()
    {
        $pageBuilder = app()->make('phpPageBuilder');
        $pageBuilder->handlePublicRequest();
    }
}
