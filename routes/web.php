<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PageBuilderController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','welcome');


Route::group(['namespace' => 'Admin','middleware' => 'auth','prefix' => 'admin'],function(){
	Route::get('/',[AdminController::class,'index'])->name('admin')->middleware(['can:admin']);
	//Route Rescource
	Route::resource('/user','UserController')->middleware(['can:admin']);
	Route::resource('/page-builder','PageBuilderController')->middleware(['can:admin']);
});

Route::group(['namespace' => 'User','middleware' => 'auth' ,'prefix' => 'user'],function(){
	Route::get('/',[UserController::class,'index'])->name('user');
	Route::get('/profile',[ProfileController::class,'index'])->name('profile');
	Route::patch('/profile/update/{user}',[ProfileController::class,'update'])->name('profile.update');
});

Route::group(['middleware' => 'auth' ,'prefix' => 'page-builder'],function(){
	Route::get('/',[PageBuilderController::class,'index'])->name('page-builder');
	Route::any('/{id}/preview', [PageBuilderController::class,'preview'])->name('pagebuilder.view');
    Route::any('/{id}/build', [PageBuilderController::class,'build'])->name('pagebuilder.build');
    Route::any('/build', [PageBuilderController::class,'build']);
});

Route::group(['namespace' => 'Auth','middleware' => 'guest'],function(){
	Route::view('/login','auth.login')->name('login');
	Route::post('/login',[LoginController::class,'authenticate'])->name('login.post');
});

// Other
Route::view('/register','auth.register')->name('register');
Route::view('/forgot-password','auth.forgot-password')->name('forgot-password');
Route::post('/logout',function(){
	return redirect()->to('/login')->with(Auth::logout());
})->name('logout');

Route::any('{uri}', [
    'uses' => 'Admin\PageBuilderController@uri',
    'as' => 'page',
])->where('uri', '.*');

